﻿using Christoc.Modules.VacationModule.BD;
using Christoc.Modules.VacationModule.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Christoc.Modules.VacationModule.DL
{
    public class DataAccess
    {
        string Query = string.Empty;
        SqlParameter[] sqlParameter = null;
        string connectionString = string.Empty;
        ConnectionBD connectionBD = new ConnectionBD();

        public DataAccess()
        {
            connectionString = connectionBD.GetConnectionLocalString(connectionBD.TestEnvionment);
        }

        public Response CreateTicketVacationRequest(VacationRequest request)
        {
            Query = @" INSERT INTO LiveHelpdesk_Issue
                        (ModuleID
                        , ChannelID
                        , Email
                        , Name
                        , Subject
                        , Message
                        , Attachments
                        , StatusID
                        , AgentID
                        , CreatedOn)
                    VALUES
                        (@ModuleID,  
                        @ChannelID,
                        @Email,
                        @Name,
                        @Subject,
                        @Message,
                        @Attachments,
                        @StatusID,
                        @AgentID,
                        (select DATEADD(HOUR, 6, GETDATE())));";

            sqlParameter = new SqlParameter[]
                {
                    new SqlParameter("@ModuleID", request.ModuleID),
                    new SqlParameter("@ChannelID", request.ChannelID),
                    new SqlParameter("@Email", request.EmailUser),
                    new SqlParameter("@Name", request.NameEmployee),
                    new SqlParameter("@Subject" , request.Subject),
                    new SqlParameter("@Message", request.Message),
                    new SqlParameter("@Attachments", ""),
                    new SqlParameter("@StatusID", request.StatusID),
                    new SqlParameter("@AgentID",request.AgentID)// Asignación a Carlos Suarez con ID=6
                };

            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var cmd = new SqlCommand(Query, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddRange(sqlParameter);
                        connection.Open();
                        var insert = cmd.ExecuteNonQuery();
                        if (insert > 0)
                        {
                            return new Response
                            {
                                IsSuccess = true,
                                Message = "Se ha generado la solicitud de vacaciones",
                                Result = ""
                            };
                        }
                        else
                            throw new Exception("No se pudo generar la solicitud");
                    }
                }
            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message,
                    Result = ""
                };
            }
        }

        /// <summary>
        /// Metodo que recupera el id del asignado a los tickets en base a su nombre de usuario
        /// Por lo normal es Carlos Suarez, con el id 6
        /// </summary>
        /// <param name="Username">nombre del usuario</param>
        /// <returns></returns>
        public int GetUserIDAssignation(string Username)
        {
            Query = @"select UserID from Users where Username=@Username";
            int UserID = 0;
            sqlParameter = new SqlParameter[]
            {
                new SqlParameter("@Username",Username)
            };

            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var cmd = new SqlCommand(Query, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddRange(sqlParameter);
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                UserID = (int)reader["UserID"];
                            }
                            return UserID;
                        }
                        else
                            return default(int);
                    }
                }
            }
            catch (Exception e)
            {
                return default(int);
            }
        }

    }
}