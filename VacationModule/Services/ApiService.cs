﻿using Christoc.Modules.VacationModule.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Christoc.Modules.VacationModule.Services
{
    public class ApiService : HttpClient
    {
        private static readonly ApiService instance;

        private ApiService() : base()
        {
            Timeout = TimeSpan.FromMilliseconds(15000);
            MaxResponseContentBufferSize = 256000;
            BaseAddress = new Uri("http://localhost:8050/");
            DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public static ApiService GetInstance()
        {
            if (instance == null) return new ApiService();
            else return instance;
        }

        static ApiService() { }


        public async Task<AccessToken> GetToken<T>(string username, string password)
        {
            try
            {
                var response = await PostAsync("token",
                    new StringContent(string.Format(
                    "grant_type=password&username={0}&password={1}",
                    username, password),
                    Encoding.UTF8, "application/x-www-form-urlencoded"));
                var resultJSON = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<AccessToken>(resultJSON);
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<Response> GetItem<T>(string url, string idemployee, AccessToken accessToken)
        {
            try
            {
                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(accessToken.TokenType, accessToken.Token);
                var response = await GetAsync($"{url}{idemployee}");
                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = response.StatusCode.ToString()
                    };
                }
                var result = await response.Content.ReadAsStringAsync();
                var data = JsonConvert.DeserializeObject<Response>(result);
                return new Response
                {
                    IsSuccess = true,
                    Message = "Ok",
                    Result = data
                };
            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message
                };
            }
        }
    }
}