﻿using Christoc.Modules.VacationModule.Models;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace Christoc.Modules.VacationModule.Services
{
    public class DataService
    {
        private string url = string.Empty;

        public string GenerateRequest(VacationRequest request)
        {
            MemoryStream memoryStream = new MemoryStream();
            string base64 = string.Empty;
            Font _standardFont = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
            Font _HeaderFont = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD, BaseColor.BLUE);
            Font _TitleFont = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD, BaseColor.BLACK);

            url = HttpContext.Current.Server.MapPath("~");

            Document document = new Document(PageSize.LETTER);
            PdfWriter pdfWriter = PdfWriter.GetInstance(document, memoryStream);
            document.Open();

            var urlImage = string.Format("{0}{1}", url, "\\DesktopModules\\MVC\\VacationModule\\Images\\logoTasi.png");

            Image image = Image.GetInstance(urlImage);
            image.BorderWidth = 0;
            image.Alignment = Element.ALIGN_LEFT;
            float percentage = 0.0f;
            percentage = 150 / image.Width;
            image.ScalePercent(percentage * 100);
            document.Add(image);

            document.AddTitle("Solicitud de Vacaciones");

            var paragraphCompanyName = new Paragraph("TASI SOFTWARE, S.A. DE C.V.", _HeaderFont);
            paragraphCompanyName.Alignment = Element.ALIGN_RIGHT;
            document.Add(paragraphCompanyName);
            document.Add(Chunk.NEWLINE);

            var paragraphTitle = new Paragraph("Solicitud de Vacaciones", _TitleFont);
            paragraphTitle.Alignment = Element.ALIGN_CENTER;
            document.Add(new Paragraph(paragraphTitle));
            document.Add(Chunk.NEWLINE);

            document.Add(new Paragraph($"No Empleado: {request.IDEmployee}"));
            document.Add(new Paragraph($"Nombre: {request.NameEmployee}"));
            document.Add(new Paragraph($"Telefono Lugar de Trabajo: {request.TelefonoOfi}"));
            document.Add(new Paragraph($"Extensión: {request.Extension}"));
            document.Add(new Paragraph($"Telefono Móvil: {request.TelefonoMov}"));
            document.Add(Chunk.NEWLINE);
            var paragraphDateRequest = new Paragraph($"Fecha Solicitud: {DateTime.Now.ToString("dd/MM/yyyy")}");
            paragraphDateRequest.Alignment = Element.ALIGN_RIGHT;
            document.Add(new Paragraph(paragraphDateRequest));
            document.Add(Chunk.NEWLINE);
            document.Add(new Paragraph("Por Medio de la presente solicitud me sean otorgados los siguientes días:"));

            PdfPTable pdfPTable = new PdfPTable(2);
            pdfPTable.WidthPercentage = 100;

            // Configuramos el título de las columnas de la tabla
            PdfPCell cl1 = new PdfPCell(new Phrase("", _standardFont));
            cl1.BorderWidth = 0;
            cl1.BorderWidthBottom = 0.75f;

            PdfPCell cl2 = new PdfPCell(new Phrase("", _standardFont));
            cl2.BorderWidth = 0;
            cl2.BorderWidthBottom = 0.75f;

            // Añadimos las celdas a la tabla
            pdfPTable.AddCell(cl1);
            pdfPTable.AddCell(cl2);

            StringBuilder stringBuilder = new StringBuilder();
            foreach (var i in request.DiasTomar)
            {
                stringBuilder.AppendLine();
                stringBuilder.Append(i);
                stringBuilder.AppendLine();
            }

            // Llenamos la tabla con información
            cl1 = new PdfPCell(new Phrase("Vacaciones", _standardFont));
            cl1.BorderWidth = 1;
            pdfPTable.AddCell(cl1);
            string strText = string.Empty;
            if (request.AniversarioCumplido)
            {
                strText = $"Total, de días solicitados: {request.CantidadDias}\n\nFechas:\n {stringBuilder}";
            }
            if (!request.AniversarioCumplido)
            {
                strText = $"Total, de días solicitados: _____\n\nFechas:\n";
            }


            cl2 = new PdfPCell(new Phrase($"{strText}\nDel periodo correspondiente a mi fecha de aniversario dd/mm/yyyy",
               _standardFont));
            cl2.BorderWidth = 1;
            pdfPTable.AddCell(cl2);

            // Añadimos las celdas a la tabla
            cl1 = new PdfPCell(new Phrase("Permiso a cuenta de Vacaciones", _standardFont));
            cl1.BorderWidth = 1;
            pdfPTable.AddCell(cl1);

            strText = string.Empty;
            if (request.AniversarioNoCumplido)
            {
                strText = $"Total, de días solicitados: {request.CantidadDias}\n\nFechas:\n{stringBuilder}";
            }
            if (!request.AniversarioNoCumplido)
            {
                strText = $"Total, de días solicitados: _____\n\nFechas:\n";
            }

            cl2 = new PdfPCell(new Phrase($"{strText}\nDel periodo correspondiente a mi proxima fecha de aniversario dd/mm/yyyy",
                _standardFont));
            cl2.BorderWidth = 1;
            pdfPTable.AddCell(cl2);
            
            PdfPTable tblFirma = new PdfPTable(3);
            tblFirma.WidthPercentage = 100;

            // Configuramos el título de las columnas de la tabla


            PdfPCell clSolicito = new PdfPCell(new Phrase("SOLICITO", _standardFont));
            clSolicito.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            clSolicito.BorderWidth = 1;
            clSolicito.BorderWidthBottom = 0.75f;
            clSolicito.BackgroundColor = BaseColor.LIGHT_GRAY;

            PdfPCell clTasisoft = new PdfPCell(new Phrase("VO. BO.", _standardFont));
            clTasisoft.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            clTasisoft.BorderWidth = 1;
            clTasisoft.BorderWidthBottom = 0.75f;
            clTasisoft.BackgroundColor = BaseColor.LIGHT_GRAY;

            PdfPCell clAutorizo = new PdfPCell(new Phrase("AUTORIZO", _standardFont));
            clAutorizo.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            clAutorizo.BorderWidth = 1;
            clAutorizo.BorderWidthBottom = 0.75f;
            clAutorizo.BackgroundColor = BaseColor.LIGHT_GRAY;

            // Añadimos las celdas a la tabla
            tblFirma.AddCell(clSolicito);
            tblFirma.AddCell(clTasisoft);
            tblFirma.AddCell(clAutorizo);


            clSolicito = new PdfPCell(new Phrase("", _standardFont));
            clSolicito.BorderWidth = 1;
            clSolicito.MinimumHeight = 30;
            clSolicito.BorderWidthBottom = 0.75f;

            clTasisoft = new PdfPCell(new Phrase("", _standardFont));
            clTasisoft.BorderWidth = 1;
            clTasisoft.MinimumHeight = 30;
            clTasisoft.BorderWidthBottom = 0.75f;

            clAutorizo = new PdfPCell(new Phrase("", _standardFont));
            clAutorizo.BorderWidth = 1;
            clAutorizo.MinimumHeight = 30;
            clAutorizo.BorderWidthBottom = 0.75f;

            // Añadimos las celdas a la tabla
            tblFirma.AddCell(clSolicito);
            tblFirma.AddCell(clTasisoft);
            tblFirma.AddCell(clAutorizo);

            // Llenamos la tabla con información
 
            clSolicito = new PdfPCell(new Phrase($"NOMBRE COLABORADOR\n{request.EmailUser}", _standardFont));
            clSolicito.BorderWidth = 1;
            clSolicito.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
 
            clTasisoft = new PdfPCell(new Phrase($"NOMBRE Y FIRMA DEL SUPERVISOR TASISOFT\n{request.EmailSupervisor}", _standardFont));
            clTasisoft.BorderWidth = 1;
            clTasisoft.HorizontalAlignment = PdfPCell.ALIGN_CENTER;

            clAutorizo = new PdfPCell(new Phrase($"NOMBRE Y FIRMA DEL JEFE DIRECTO\n{request.EmailJefeDirecto}", _standardFont));
            clAutorizo.BorderWidth = 1;
            clAutorizo.HorizontalAlignment = PdfPCell.ALIGN_CENTER;

            // Añadimos las celdas a la tabla
            tblFirma.AddCell(clSolicito);
            tblFirma.AddCell(clTasisoft);
            tblFirma.AddCell(clAutorizo);
            
            document.Add(Chunk.NEWLINE);
            document.Add(pdfPTable);

            document.Add(Chunk.NEWLINE);
            document.Add(new Paragraph("PRESENTANDOME A MIS LABORES EL DÍA DD/MM/YYYY"));

            document.Add(Chunk.NEWLINE);
            document.Add(tblFirma);

       
            document.Close();
            pdfWriter.Close();
            var byteArray = memoryStream.ToArray();
            base64 = Convert.ToBase64String(byteArray);
            return base64;
        }


        /// <summary>
        /// Metodo que recupera el id del modulo de HelpDesk (Tickets) para poder obtener o crear la carpeta
        /// en donde se guardaran los archivos.
        /// </summary>
        /// <returns></returns>
        public static int GetModuleID()
        {
            int moduleID = 0;
            var ModuleInfo = DotNetNuke.Entities.Modules.ModuleController.Instance.GetAllModules();
            foreach (var mod in ModuleInfo)
            {
                var moduleInfo = (DotNetNuke.Entities.Modules.ModuleInfo)mod;
                if (moduleInfo.ModuleTitle.Equals("Tickets"))
                {
                    moduleID = moduleInfo.ModuleID;
                    break;
                }
            }
            return moduleID;
        }
    }
}