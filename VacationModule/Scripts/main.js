﻿var url = "/tasinet_test/DesktopModules/VacationModule/API/VacationApi/";

var obj = {
    TelefonoOfi: '',
    Extension: '',
    TelefonoMov: '',
    AniversarioCumplido: false,
    AniversarioNoCumplido: false,
    CantidadDias: 0,
    DiasTomar: new Array(),
    NombreJefeDirecto: '',
    EmailJefeDirecto: '',
    NombreSupervisor: '',
    EmailSupervisor: ''
};

$(function () {

    Token("/DesktopModules/VacationModule/API/VacationApi/GetRequestApi", {});
    // Automatically add a first row of data
    $('#addRow').click();

    $('#tb_vacation').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"     
        }
    });

    $('#example').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
    });

    $('#example tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            $("#example").DataTable().$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });
  

    $('#chk_aniversarioCumplido').bootstrapToggle('off');
    $('#chk_aniversarioNoCumplido').bootstrapToggle('off');

    $('.phone').mask('000-0000000');
    $('.phone_with_ddd').mask('(00) 0000-0000');
    $('.extension').mask('000');
 
    $("#txt_emailJefeDirecto").data("emailSupervisorOk", 0);
    $("#txt_emailJefeDirecto").data("emailJefeDirectoOk", 0);

    $('.datepicker').datepicker({
        language: 'es-ES',
        autoHide: true
    });
 

    $("#txt_cantidadDiasSolicitados").change(function () {
        if (parseInt($(this).val()) < 0) {
            $(this).val(0);
        }
    })

    $("#btn_agregarFecha").click(function () {
        var day = ($("#txt_fechasATomar").val() != "") ? $("#txt_fechasATomar").val() : "";
        var dDate = new Date(parseInt(day.split('/')[2]), parseInt(day.split('/')[0]) - 1, parseInt(day.split('/')[1]));
        var dDateI = null;
        var dNow = new Date();
        var numDays = parseInt($("#txt_cantidadDiasSolicitados").val());

        if (dDate.valueOf() < dNow.valueOf()) {
            alert("La fecha solicitada debe ser mayor a la fecha actual");
            return;
        }

        if (numDays == 0) {
            alert("No se ha ingresado el número de días a tomar");
            return;
        }
        
        if (day == "") {
            alert("No se ha seleccionada ninguna fecha");
            return;
        }

        var countTable = $("#example").DataTable().data().count();
        if (countTable < numDays) {

            var table = $('#example').DataTable();

            table
                .column(0)
                .data()
                .each(function (value, index) {
                    console.log('Data in index: ' + index + ' is: ' + value);
                    dDateI = new Date(parseInt(value.split('/')[2]), parseInt(value.split('/')[0]) - 1, parseInt(value.split('/')[1]));
                });

            if (dDateI != null) {
                if (dDate.valueOf() == dDateI.valueOf()) {
                    alert("Ya existe esta fecha agregada");
                    return;
                }
            }

            $("#example").DataTable().row.add([
                day
            ]).draw(false);

            $("#txt_fechasATomar").val("");

        }
        else {
            alert("El numero de fechas no puede ser mayor al de los dias solicitados");
        }
    });


    $('#chk_aniversarioCumplido').change(function () {
        obj.AniversarioCumplido = $(this).prop('checked');
        //if (obj.AniversarioCumplido) {
        //    $('#chk_aniversarioNoCumplido').bootstrapToggle('off');
        //    obj.AniversarioNoCumplido = !$("#chk_aniversarioNoCumplido").prop('checked');
        //}
    });

    $('#chk_aniversarioNoCumplido').change(function () {
        obj.AniversarioNoCumplido = $(this).prop('checked');
        //if (obj.AniversarioNoCumplido) {
        //    $('#chk_aniversarioCumplido').bootstrapToggle('off');
        //    obj.AniversarioCumplido = !$("#chk_aniversarioCumplido").prop('checked');
        //}
    });

    $("#btn_generarSolicitud").click(function () {

        obj.TelefonoOfi = ($("#txt_telefonoOficina").val() != "") ? $("#txt_telefonoOficina").val() : "";
        obj.Extension = ($("#txt_extension").val() != "") ? $("#txt_extension").val() : "";
        obj.TelefonoMov = ($("#txt_telefonoMovil").val() != "") ? $("#txt_telefonoMovil").val() : "";
        obj.CantidadDias = ($("#txt_cantidadDiasSolicitados").val() != "") ? $("#txt_cantidadDiasSolicitados").val() : "";
        obj.NombreJefeDirecto = ($("#txt_nombreJefeDirecto").val() != "") ? $("#txt_nombreJefeDirecto").val() : "";
        obj.EmailJefeDirecto = ($("#txt_emailJefeDirecto").val() != "") ? $("#txt_emailJefeDirecto").val() : "";
        obj.NombreSupervisor = ($("#txt_nombreSupervisor").val() != "") ? $("#txt_nombreSupervisor").val() : "";
        obj.EmailSupervisor = ($("#txt_emailSupervisor").val() != "") ? $("#txt_emailSupervisor").val() : "";

        var arrayDays = new Array();
        var table = $('#example').DataTable();

        table
            .column(0)
            .data()
            .each(function (value, index) {
                console.log('Data in index: ' + index + ' is: ' + value);
                arrayDays.push(value);
            });

        if (!obj.AniversarioCumplido && !obj.AniversarioNoCumplido) {
            alert("Debe seleccionar una opción de solicitud");
            return;
        }

        if (arrayDays.length == 0) {
            alert("No se encontrarón fechas");
            return;
        }
        obj.DiasTomar = arrayDays;

        if (obj.EmailSupervisor == 0) {
            alert("El correo del supervisor no es correcto");
            return;
        }
        if (obj.EmailJefeDirecto == 0) {
            alert("El correo del jefe directo no es correcto");
            return;
        }

        if (obj.AniversarioCumplido && obj.AniversarioNoCumplido) {
            alert("Solo se debe se elegír una opción: ANIVERSARIO CUMPLIDO o PERMISO A CUENTA DE VACACIONES");
            return;
        }

        if (arrayDays.length < obj.CantidadDias) {
            alert("Los días solicitados no coinciden con los días a tomar seleccionados");
            return;
        }

        GenerateVacationRequest(url + 'GenerateVacationRequest', JSON.stringify(obj));

    });
});


function GenerateVacationRequest(url, data) {
    $.ajax({
        url: url,
        cache: false,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: data,
        beforeSend: function () {
            $(".alert").html("Generando Solicitud");
            $(".alert").addClass("alert-info");
        },
        success: function (response) {
            $(".alert").html("");
            $(".alert").removeClass("alert-info");
            if (response.IsSuccess) {
                var currentUrl = window.location.href.split('action');
                var newUrl = currentUrl[0] + "action/DownloadRequest";
                window.location.href = newUrl;
                $(".alert").html(response.Message);
                $(".alert").addClass("alert-success");
                ClearFields();
            }
            else {
                $(".alert").html("No se pudo generar la solicitud");
                $(".alert").addClass("alert-danger");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.responseText);
            alert(xhr.responseText);
            $(".alert").html("");
            $(".alert").removeClass("alert-info");
        },
        complete: function () {
           
        }
    });
}


function Token(url, data) {
    $.ajax({
        url: url,
        cache: false,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        data: data,
        beforeSend: function () {
             
        },
        success: function (response) {
            console.log(response);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.responseText);
            alert(xhr.responseText);
         
        },
        complete: function () {

        }
    });
}

function validarLetras(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = "8-37-39-46";

    tecla_especial = false
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
        return false;
    }
}


function validarEmail(email) 
{
    var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email) ? true : false;
}



function validarEmailInput(id,span,data) {
    var email = $("#" + id).val();
    var domain = email.split('@')[1];
    if (validarEmail(email)) {
        var com = domain.split('.');
        if (com.length > 3) {
            $("#" + span).removeClass("text-success").addClass("text-danger");
            $("#" + span).html("El formato de correo no es correcto");
            $("#" + id).data(data, 0);
            return;
        }
        $("#" + span).removeClass("text-danger").addClass("text-success");
        $("#" + span).html("El formato de correo es correcto");
        $("#" + id).data(data, 1);
    }
    else {
        $("#" + span).removeClass("text-success").addClass("text-danger");
        $("#" + span).html("El formato de correo no es correcto");
        $("#" + id).data(data, 0);
    }
}

function RemoveRow() {  
    $("#example").DataTable().row('.selected').remove().draw(false);
}

function ClearFields() {
    $("#txt_telefonoOficina").val("");
    obj.TelefonoOfi = '';

    $("#txt_extension").val("");
    obj.Extension = '';
    $("#txt_telefonoMovil").val("");
    obj.TelefonoMov = '';

    $('#chk_aniversarioCumplido').bootstrapToggle('off');
    obj.AniversarioCumplido = false;
    $('#chk_aniversarioNoCumplido').bootstrapToggle('off');
    obj.AniversarioNoCumplido = false;

    $("#txt_cantidadDiasSolicitados").val("0");
    obj.CantidadDias = 0;
    $("#txt_fechasATomar").val("");
    obj.DiasTomar = [];
    $("#txt_nombreJefeDirecto").val("");
    obj.NombreJefeDirecto = '';
    $("#txt_emailJefeDirecto").val("");
    obj.EmailJefeDirecto = '';
    $("#txt_nombreSupervisor").val("");
    obj.NombreSupervisor = '';
    $("#txt_emailSupervisor").val("");
    obj.EmailSupervisor = '';

    $("#example").DataTable()
        .clear()
        .draw();
}