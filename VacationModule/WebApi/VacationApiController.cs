﻿using Christoc.Modules.VacationModule.DL;
using Christoc.Modules.VacationModule.Models;
using Christoc.Modules.VacationModule.Services;
using DotNetNuke.Entities.Users;
using DotNetNuke.Web.Api;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Christoc.Modules.VacationModule.WebApi
{
    public class VacationApiController : DnnApiController
    {
        DataService DataService = new DataService();
        Response response = new Response();
        DataAccess DataAccess = new DataAccess();

        [AllowAnonymous]
        [HttpPost]
        public HttpResponseMessage GenerateVacationRequest([FromBody] VacationRequest request)
        {
            var user = UserController.Instance.GetCurrentUserInfo();
            try
            {
                request.IDEmployee = user.Profile.GetPropertyValue("IDEmployee");
                request.NameEmployee = string.Format("{0} {1}", user.FirstName, user.LastName);
                request.EmailUser = user.Email;

                //ticket
                request.ModuleID = DataService.GetModuleID();
                request.Subject = $"Solicitud de {request.CantidadDias} días de Vacaciones para {request.NameEmployee}";
                request.Message = @"Toda la información de la solicitud
                                Adjuntar documento PDF generado
                                solo a la espera de que el candidato complete el proceso adjuntando la autorización, 
                                al finalizar avisarle el No de Ticket generado, 
                                el procedimiento para concluir el proceso que les deje abajo.";
                request.StatusID = 3;
                request.ChannelID = 0;
                request.AgentID = DataAccess.GetUserIDAssignation("Carlos_Suarez");

                var fileBase64 = DataService.GenerateRequest(request);
                if (!string.IsNullOrEmpty(fileBase64))
                {
                    var createTicket = DataAccess.CreateTicketVacationRequest(request);
                    if (createTicket.IsSuccess)
                    {
                        response.IsSuccess = true;
                        response.Message = "Solicitud Generada[[br]][[br]]";
                        response.Message = string.Format("{0}{1}", response.Message, Message.MsgVacation);
                        response.Message = response.Message.Replace("[[br]]", "</br>");
                        MainModel.GetInstance().FileBase64 = fileBase64;
                    }
                }
                else
                {
                    response.IsSuccess = false;
                    response.Result = fileBase64;
                }
                return Request.CreateResponse(HttpStatusCode.OK, response, "application/json");
            }
            catch(Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    IsSuccess = false,
                    Message = e.Message
                }, "application/json");
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<HttpResponseMessage> GetRequestApi()
        {
            var user = UserController.Instance.GetCurrentUserInfo();
            if (string.IsNullOrEmpty(MainModel.GetInstance().AccessToken.Token) || MainModel.GetInstance().AccessToken == null)
            {
                var token = await ApiService.GetInstance().GetToken<AccessToken>(user.Username, user.PasswordResetToken.ToString());
                MainModel.GetInstance().AccessToken = token;
            }

            var data = await ApiService.GetInstance().GetItem<EmployeeVM>("api/vacation/vacationdays/byidemployee/", "TSO352", MainModel.GetInstance().AccessToken);
            if (data.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, data, "application/json");

            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, data, "application/json");
            }

        }
    }
}