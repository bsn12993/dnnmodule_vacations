﻿using Christoc.Modules.VacationModule.Models;
using DotNetNuke.Web.Mvc.Framework.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Christoc.Modules.VacationModule.Controllers
{
    public class VacationController : DnnController
    {
        // GET: Vacation
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult VacationRequest()
        {
            return View();
        }

        public FileResult DownloadRequest()
        {
            var report = MainModel.GetInstance().FileBase64;
            byte[] file = Convert.FromBase64String(report);
            return File(file, "application/pdf", "Solicitud-" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ".pdf");
        }
    }
}