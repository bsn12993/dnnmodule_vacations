﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.VacationModule.Models
{
    public class Message
    {
        public static string MsgVacation
        {
            get
            {
                return @"Estimado colega:[[br]]
                        Deseamos disfrutes los días que tomaras, por favor te recordamos que debes completar el proceso con los siguientes pasos antes de que inicies el disfrute de tus días solicitados.[[br]]
                        1.Debes por favor firmar el documento.[[br]]
                        2.Debes por favor firmar tu jefe directo en donde estas asignando. (Se permiten firmas electrónicas o emails de autorización).[[br]]
                        3.Debes por favor firmar tu supervisor de TASISOFT(Aplica si es diferente al punto 2). (Se permiten firmas electrónicas o emails de autorización).[[br]]
                        4.Debes por favor levantar un ticket para el área de Recursos Humanos donde adjuntes este documento.[[br]]
                        5.Debes esperas que formalmente te atiendan tu ticket.[[br]]
                        6.Muchas gracias.";
            }
        }
    }
}