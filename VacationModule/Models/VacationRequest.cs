﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.VacationModule.Models
{
    public class VacationRequest
    {

        public string IDEmployee { get; set; }
        public string NameEmployee { get; set; }

        public string TelefonoOfi { get; set; }
        public string Extension { get; set; }
        public string TelefonoMov { get; set; }
        public bool AniversarioCumplido { get; set; }
        public bool AniversarioNoCumplido { get; set; }
        public int CantidadDias { get; set; }
        public List<string> DiasTomar { get; set; }
        public string NombreJefeDirecto { get; set; }
        public string EmailJefeDirecto { get; set; }
        public string NombreSupervisor { get; set; }
        public string EmailSupervisor { get; set; }

        public int ModuleID { get; set; }
        public string EmailUser { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public object Attachment { get; set; }
        public int StatusID { get; set; }
        public int AgentID { get; set; }
        public int ChannelID { get; set; }


        public VacationRequest()
        {
            this.IDEmployee = string.Empty;
            this.NameEmployee = string.Empty;

            this.TelefonoOfi = string.Empty;
            this.Extension = string.Empty;
            this.TelefonoMov = string.Empty;
            this.AniversarioCumplido = false;
            this.AniversarioNoCumplido = false;
            this.CantidadDias = 0;
            this.DiasTomar = new List<string>();
            this.NombreJefeDirecto = string.Empty;
            this.EmailJefeDirecto = string.Empty;
            this.NombreSupervisor = string.Empty;
            this.EmailSupervisor = string.Empty;

            this.EmailUser = string.Empty;
            this.Subject = string.Empty;
            this.Message = string.Empty;

        }

    }
}