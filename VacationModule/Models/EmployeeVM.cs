﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.VacationModule.Models
{
    public class EmployeeVM
    {
        public string IDEmployee { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime CreatedDate { get; set; }
        public double VacationDays { get; set; }
        public int YearsWork { get; set; }

        public string FullName
        {
            get
            {
                return string.Format("{0} {1} {2}", this.Name, this.FirstName, this.LastName);
            }
        }

        public EmployeeVM()
        {
            this.IDEmployee = string.Empty;
            this.Name = string.Empty;
            this.FirstName = string.Empty;
            this.LastName = string.Empty;
            this.CreatedDate = DateTime.MinValue;
            this.VacationDays = 0;
            this.YearsWork = 0;
        }
    }
}