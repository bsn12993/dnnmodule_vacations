﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.VacationModule.Models
{
    public static class HTMLTemplate
    {
        public static string Header { get; set; }
        public static string Body_Data
        {
            get
            {
                return @"<P LANG='es-ES' CLASS='western' ALIGN='CENTER'>
                    <FONT FACE='Arial, sans-serif'><FONT SIZE='3'><B>SOLICITUD DE VACACIONES</B></FONT></FONT></P>
                    <P LANG='es-ES' CLASS='western' ALIGN=CENTER><BR></P>
                    <P LANG='es-ES' CLASS='western' ALIGN=CENTER><FONT SIZE=3><FONT FACE='Verdana, sans-serif'>No Empleado: [[NOEMP]]</FONT></FONT></P>
                    <P LANG='es-ES' CLASS='western' ALIGN=CENTER><FONT SIZE=3><FONT FACE='Verdana, sans-serif'>Nombre: [[NOMBRE]] </FONT></FONT></P>
                    <P LANG='es-ES' CLASS='western' ALIGN=CENTER><FONT SIZE=3><FONT FACE='Verdana, sans-serif'>Teléfono Lugar de Trabajo: [[TEL]] Ext. [[EXT]]</FONT></FONT></P>
                    <P LANG='es-ES' CLASS='western' ALIGN=CENTER><FONT SIZE=3><FONT FACE='Verdana, sans-serif'>Teléfono Móvil: [[MOVIL]] Ext.</FONT></FONT></P>
                    <P LANG='es-MX' CLASS='western' ALIGN=CENTER><BR></P>
                    <P LANG='es-MX' CLASS='western' ALIGN=CENTER><BR></P>
                    <P LANG='es-ES' CLASS='western' ALIGN=CENTER><FONT SIZE=3><FONT FACE='Arial, sans-serif'>Fecha Solicitud: [[FCHSOL]] </FONT></FONT></P>
                    <P LANG='es-ES' CLASS='western' ALIGN=CENTER><BR></P>
                    <P LANG='es-ES' CLASS='western' ALIGN=JUSTIFY><FONT SIZE=3>                                                                      
                        <FONT FACE='Arial, sans-serif'>Por Medio de la presente solicitud me sean otorgados los siguientes días:</FONT></FONT>
                    </P>";
            }
        }

        public static string Body_Days
        {
            get
            {
                return @"<TABLE WIDTH=656 CELLPADDING=7 CELLSPACING=0>
	                    <COL WIDTH=181>
	                    <COL WIDTH=445>
	                        <TR VALIGN=TOP>
		                        <TD WIDTH=181 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			                        <P LANG='es-ES' CLASS='western' ALIGN=JUSTIFY><FONT SIZE=3><FONT FACE='Arial, sans-serif'>Vacaciones</FONT></FONT></P>
                                </TD>
                                <TD WIDTH=445 STYLE='border: 1px solid #000000; padding: 0in 0.08in'>
                                    <P LANG='es-ES' CLASS='western' ALIGN=JUSTIFY><FONT SIZE=3><FONT FACE='Arial, sans-serif'>Total, de Días Solicitados: ______________ </FONT></FONT></P>
                                    <P LANG='es-ES' CLASS='western' ALIGN=JUSTIFY><BR>
                                        </P>
                                        <P LANG='es-ES' CLASS='western' ALIGN=JUSTIFY><FONT SIZE=3><FONT FACE='Arial, sans-serif'> Fechas:dd.mm.aaaa </FONT></FONT></P>
                                <P LANG= 'es-ES' CLASS='western' ALIGN=JUSTIFY STYLE='margin-left: 0.98in'>
                                    <FONT SIZE=3><FONT FACE='Arial, sans-serif'>dd.mm.aaaa</FONT></FONT>
                                </P>
                                <P LANG='es-ES' CLASS='western' ALIGN=JUSTIFY STYLE='margin-left: 0.98in'>
                                    <FONT SIZE=3><FONT FACE='Arial, sans-serif'> dd.mm.aaaa </FONT></FONT>
                                </P>
                                <P LANG='es-ES' CLASS='western' ALIGN=JUSTIFY STYLE='margin-left: 0.98in'>
                                    <FONT SIZE=3><FONT FACE='Arial, sans-serif'> dd.mm.aaaa </FONT></FONT>
                                </P>
                                <P LANG='es-ES' CLASS='western' ALIGN=JUSTIFY STYLE='margin-left: 0.98in'>
                                    <BR>
                                </P>
                                <P LANG='es-ES' CLASS='western' ALIGN=JUSTIFY><FONT SIZE=3><FONT FACE='Arial, sans-serif'> 
                                Del periodo correspondiente a mi fecha de aniversario dd.mm.aaaa </FONT></FONT>
                                </P>
                            </TD>
                        </TR>
                        <TR VALIGN=TOP>
                                <TD WIDTH=181 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
                                    <P LANG='es-ES' CLASS='western' ALIGN=JUSTIFY><FONT SIZE=3><FONT FACE='Arial, sans-serif'> 
                                    Permiso a cuenta de Vacaciones</FONT></FONT></P>
                            </TD>
                            <TD WIDTH=445 STYLE='border: 1px solid #000000; padding: 0in 0.08in'>
                                    <P LANG='es-ES' CLASS='western' ALIGN=JUSTIFY><FONT SIZE=3><FONT FACE='Arial, sans-serif'> 
                                    Total, de Días Solicitados: ______________ </ FONT ></ FONT ></ P >
                                <P LANG='es-ES' CLASS='western' ALIGN=JUSTIFY> BR>
                                </P>
                                <P LANG='es-ES' CLASS='western' ALIGN=JUSTIFY><FONT SIZE=3><FONT FACE='Arial, sans-serif'>
                                    Fechas: dd.mm.aaaa </ FONT ></ FONT ></ P >
                                <P LANG='es-ES' CLASS='western' ALIGN=JUSTIFY STYLE='margin-left: 0.98in'>
                                    <FONT SIZE=3><FONT FACE='Arial, sans-serif'> dd.mm.aaaa </FONT></FONT>
                                </P>
                                <P LANG='es-ES' CLASS='western' ALIGN=JUSTIFY STYLE='margin-left: 0.98in'>
                                    <FONT SIZE=3><FONT FACE='Arial, sans-serif'> dd.mm.aaaa </FONT></FONT>
                                </P>
                                <P LANG='es-ES' CLASS='western' ALIGN=JUSTIFY STYLE='margin-left: 0.98in'>                               
                                    <FONT SIZE=3><FONT FACE='Arial, sans-serif'> dd.mm.aaaa </FONT></FONT>
                                </P>                                 
                                    <P LANG='es-ES' CLASS='western' ALIGN=JUSTIFY STYLE ='margin-left: 0.98in'>                                 
                                    <BR>                                          
                                </P>
                                <P LANG='es-ES' CLASS='western' ALIGN=JUSTIFY><FONT SIZE=3><FONT FACE='Arial, sans-serif'> 
                                    Del periodo correspondiente a mi próxima fecha de aniversario dd.mm.aaaa </FONT></FONT>
                                </P>
                            </TD>
                        </TR>
                    </TABLE>";
            }
        }
        public static string Body_Sign
        {
            get
            {
                return @"<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>
                        <P LANG='es-ES' CLASS='western' ALIGN=JUSTIFY></P>
                        <P LANG='es-ES' CLASS='western' ALIGN=LEFT><FONT SIZE=3><FONT FACE='Arial, sans-serif'> 
                           
                            PRESENTANDOME A MIS LABORES EL DÍA DD.MM.AAAA </FONT></FONT>
                        </P>
                        <TABLE WIDTH=686 CELLPADDING=5 CELLSPACING=0>
                            <COL WIDTH=99>
                            <COL WIDTH=50>
                            <COL WIDTH=33>
                            <COL WIDTH=1>         
                            <COL WIDTH=72>
                            <COL WIDTH=50>
                            <COL WIDTH=83>
                            <COL WIDTH=1>
                            <COL WIDTH=72>
                            <COL WIDTH=58>
                            <COL WIDTH=57>
                            <TR VALIGN=BOTTOM>               
                                <TD WIDTH=99 HEIGHT=9 STYLE='border: none; padding: 0in'>
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>
                                    </P>                           
                                </TD>
                                <TD WIDTH=50 STYLE='border: none; padding: 0in'>                             
                                    <P LANG='es-MX' CLASS='western' ALIGN=CENTER><BR>               
                                    </P>
                                </TD>
                                <TD WIDTH=33 STYLE='border: none; padding: 0in'>
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR> 
                                    </P>       
                                </TD>
                                <TD WIDTH=1 STYLE='border: none; padding: 0in'>                                              
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>                                                 
                                    </P>                                                 
                                </TD>                                                   
                                <TD WIDTH=72 STYLE='border: none; padding: 0in'>                                                     
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>
                                    </P>                                                           
                                </TD>                                                          
                                <TD WIDTH=50 STYLE='border: none; padding: 0in'>                                                           
                                <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>                                                                   
                                    </P>                                                                   
                                </TD>                                                                   
                                <TD WIDTH=83 STYLE='border: none; padding: 0in'>                                                                     
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>                                                                           
                                    </P>                                                                          
                                </TD>                                                                           
                                <TD WIDTH=1 STYLE='border: none; padding: 0in'>                                                                             
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>                                                                                   
                                    </P>                                                                                  
                                </TD>                                                         
                                <TD WIDTH=72 STYLE='border: none; padding: 0in'>                                                                                     
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>                                                                    
                                    </P>                                                                 
                                </TD>                                                                
                                <TD WIDTH=58 STYLE='border: none; padding: 0in'>                                                                    
                                <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>                                                                                                  
                                    </P>                                                                   
                                </TD>                                                                           
                                <TD WIDTH=57 STYLE='border: none; padding: 0in'>                                                                                                  
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>                                                                                                         
                                    </P>                                                                                      
                                </TD>                                                                                          
                            </TR>                                                                                               
                            <TR VALIGN=BOTTOM>                                                                                                           
                                <TD COLSPAN=3 WIDTH=203 HEIGHT=9 BGCOLOR='#c0c0c0' STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in'>                                                                                                                   
                                    <P LANG='es-MX' CLASS='western' ALIGN=CENTER><FONT FACE='Arial, sans-serif'><FONT SIZE=2><B> SOLICITO </B></FONT></FONT></P>                                                                                                    
                                </TD>                                                                             
                                <TD WIDTH=1 STYLE='border-top: none; border-bottom: none; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in'>                                                                                                                                    
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>                                                                                                                                          
                                    </P>                                                                                                                                         
                                </TD>                                                                                                                                          
                                <TD COLSPAN=3 WIDTH=225 BGCOLOR='#c0c0c0' STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in'>                                                                                                                                                
                                    <P LANG='es-MX' CLASS='western' ALIGN=CENTER><FONT FACE='Arial, sans-serif'><FONT SIZE=2><B> Vo. Bo.</B></FONT></FONT></P>                                                                                                                                                              
                                </TD>                                                                                                                                                              
                                <TD WIDTH=1 STYLE='border-top: none; border-bottom: none; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in'>                                                                                                                                                                
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>                                                                                                                                                                      
                                    </P>                                                                                                                                                                      
                                </TD>                                                                                                                                                                      
                                <TD COLSPAN=3 WIDTH=207 BGCOLOR='#c0c0c0' STYLE='border: 1px solid #000000; padding: 0in 0.05in'>                                                                                                                                                                            
                                    <P LANG='es-MX' CLASS='western' ALIGN=CENTER><FONT FACE='Arial, sans-serif'><FONT SIZE=2><B> AUTORIZO </B></FONT></FONT></P>                                                                                                                                                                                          
                                </TD>                                                                                                                                                                                          
                            </TR>                                                                                                                                                                                          
                            <TR VALIGN=BOTTOM>                                                                                                                                                                                           
                                <TD WIDTH=99 HEIGHT=9 STYLE='border-top: none; border-bottom: none; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in'>                                                                                                                                                                                                
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT> &nbsp;</P>                                                                                                                                                                                                         
                                </TD>                                                                                                                                                                                                         
                                <TD WIDTH=50 STYLE='border: none; padding: 0in'>                                                                                                                                                                                                            
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>                                                                                                                                                                                                                 
                                    </P>                                                                                                                                                                                                                 
                                </TD>                                                                                                                                                                                                                
                                <TD WIDTH=33 STYLE='border: none; padding: 0in'>                                                                                                                                                                                                                   
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT> &nbsp;</P>                                                                                                                                                                                                                            
                                </TD>                                                                                                                                                                                                                            
                                <TD WIDTH=1 STYLE='border-top: none; border-bottom: none; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in'>                                                                                                                                                                                                                                
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>                                                                                                                                                                                                                                     
                                    </P>                                                                                                                                                                                                                                     
                                </TD>                                                                                                                                                                                                                                     
                                <TD WIDTH=72 STYLE='border-top: none; border-bottom: none; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in'>                                                                                                                                                                                                                                        
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT> &nbsp;</P>                                                                                                                                                                                                                                                 
                                </TD>                                                                                                                                                                                                                                                 
                                <TD WIDTH=50 STYLE='border: none; padding: 0in'>                                                                                                                                                                                                                                                    
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>                                                                                                                                                                                                                                                         
                                    </P>                                                                                                                                                                                                                                                         
                                </TD>                                                                                                                                                                                                                                                         
                                <TD WIDTH=83 STYLE='border: none; padding: 0in¿>                                                                                                                                                                                                                                                            
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT> &nbsp;</P>                                                                                                                                                                                                                                                                     
                                </TD>                                                                                                                                                                                                                                                                     
                                <TD WIDTH=1 STYLE='border-top: none; border-bottom: none; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in'>                                                                                                                                                                                                                                                                       
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>                                                                                                                                                                                                                                                                            
                                    </P>                                                                                                                                                                                                                                                                             
                                </TD>                                                                                                                                                                                                                                                                             
                                <TD WIDTH=72 STYLE='border-top: none; border-bottom: none; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in'>                                                                                                                                                                                                                                                                                
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT> &nbsp;</P>                                                                                                                                                                                                                                                                                         
                                </TD>                                                                                                                                                                                                                                                                                         
                                <TD WIDTH=58 STYLE='border: none; padding: 0in'>                                                                                                                                                                                                                                                                                       
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>                                                                                                                                                                                                                                                                                               
                                    </P>                                                                                                                                                                                                                                                                                                
                                </TD>                                                                                                                                                                                                                                                                                                 
                                <TD WIDTH=57 STYLE='border - top: none; border - bottom: none; border - left: none; border - right: 1px solid #000000; padding-top: 0in; padding-bottom: 0in; padding-left: 0in; padding-right: 0.05in'>                                                                                                                                                                                                                                                                                                   
                                    <P LANG=0es-MX' CLASS='western' ALIGN=LEFT> &nbsp;</P>
                                </TD>
                            </TR>
                            <TR VALIGN=BOTTOM>
                                <TD WIDTH=99 HEIGHT=9 STYLE='border-top: none; border-bottom: none; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in'>
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT> &nbsp;</P>
                                </TD>
                                <TD WIDTH=50 STYLE='border: none; padding: 0in'>
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>
                                    </P>
                                </TD>
                                <TD WIDTH=33 STYLE='border: none; padding: 0in'>
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT> &nbsp;</P>
                                </TD>
                                <TD WIDTH=1 STYLE='border-top: none; border-bottom: none; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in'>
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>
                                    </P>
                                </TD>
                                <TD WIDTH=72 STYLE='border-top: none; border-bottom: none; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in'>
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT> &nbsp;</P>
                                </TD>
                                <TD WIDTH=50 STYLE='border: none; padding: 0in'>
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>
                                    </P>
                                </TD>
                                <TD WIDTH=83 STYLE='border: none; padding: 0in'>
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT> &nbsp;</P>
                                </TD>
                                <TD WIDTH=1 STYLE='border-top: none; border-bottom: none; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in'>
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>
                                    </P>
                                </TD>
                                <TD WIDTH=72 STYLE='border-top: none; border-bottom: none; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in'>
                                    <P LANG='es-MX' CLASS = 'western' ALIGN = LEFT > &nbsp;</ P >
                                </TD>
                                <TD WIDTH=58 STYLE='border: none; padding: 0in'>
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>
                                    </P>
                                </TD>
                                <TD WIDTH=57 STYLE='border-top: none; border-bottom: none; border-left: none; border-right: 1px solid #000000; padding-top: 0in; padding-bottom: 0in; padding-left: 0in; padding-right: 0.05in'>
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT> &nbsp;</P>
                                </TD>
                            </TR>
                            <TR VALIGN=BOTTOM>
                                <TD WIDTH=99 STYLE='border-top: none; border-bottom: none; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in' >
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT> &nbsp;</P>
                                </TD>
                                <TD WIDTH=50 STYLE='border: none; padding: 0in' >
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>
                                    </P>
                                </TD>
                                <TD WIDTH=33 STYLE='border: none; padding: 0in' >
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT> &nbsp;</P>
                                </TD>
                                <TD WIDTH=1 STYLE='border-top: none; border-bottom: none; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in' >
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>
                                    </P>
                                </TD>
                                <TD WIDTH=72 STYLE='border-top: none; border-bottom: none; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in' >
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT> &nbsp;</P>
                                </TD>
                                <TD WIDTH=50 STYLE='border: none; padding: 0in' >
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>
                                    </P>
                                </TD>
                                <TD WIDTH=83 STYLE='border: none; padding: 0in' >
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT> &nbsp;</P>
                                </TD>
                                <TD WIDTH=1 STYLE='border-top: none; border-bottom: none; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in' >
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>
                                    </P>
                                </TD>
                                <TD WIDTH=72 STYLE='border-top: none; border-bottom: none; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in' >
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT> &nbsp;</P>
                                </TD>
                                <TD WIDTH=58 STYLE='border: none; padding: 0in' >
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>
                                    </P>
                                </TD>
                                <TD WIDTH=57 STYLE='border-top: none; border-bottom: none; border-left: none; border-right: 1px solid #000000; padding-top: 0in; padding-bottom: 0in; padding-left: 0in; padding-right: 0.05in' >
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT> &nbsp;</P>
                                </TD>
                            </TR>
                            <TR VALIGN=BOTTOM >
                                <TD COLSPAN=3 WIDTH=203 STYLE='border-top: 1px solid #000000; border-bottom: none; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in' >
                                    <P LANG='es-MX' CLASS='western' ALIGN=CENTER><FONT FACE='Arial, sans-serif'><FONT SIZE=2><B> 
                                        NOMBRE COLABORADOR </B></FONT></FONT></P>
                                    <P LANG='es-MX' CLASS='western' ALIGN=CENTER><FONT FACE='Arial, sans-serif'><FONT SIZE=2><B> email </B></FONT></FONT></P>
                                </TD>
                                <TD WIDTH=1 STYLE='border-top: none; border-bottom: none; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in' >
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>
                                    </P>
                                </TD>
                                <TD COLSPAN=3 WIDTH=225 STYLE='border-top: 1px solid #000000; border-bottom: none; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in' >
                                    <P LANG='es-ES' CLASS='western' ALIGN=CENTER><FONT SIZE=3><FONT FACE='Arial, sans-serif'><FONT SIZE=2><SPAN LANG='es-MX'><B> 
                                        NOMBRE Y FIRMA DEL SUPERVISOR TASISOFT </BR></SPAN></FONT></FONT></FONT></P>
                                    <P LANG='es-MX' CLASS='western' ALIGN=CENTER><FONT FACE='Arial, sans-serif'><FONT SIZE=2> B> email </B></FONT></FONT></P>
                                </TD>
                                <TD WIDTH=1 STYLE='border-top: none; border-bottom: none; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in' >
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>
                                    </P>
                                </TD>
                                <TD COLSPAN=3 WIDTH=207 STYLE='border-top: 1px solid #000000; border-bottom: none; border-left: 1px solid #000000; border-right: 1px solid #000000; padding: 0in 0.05in' >
                                    <P LANG='es-MX' CLASS='western' ALIGN=CENTER><BR>
                                    </P>
                                <P LANG='es-ES' CLASS='western' ALIGN=CENTER><FONT SIZE=3><FONT FACE='Arial, sans-serif'><FONT SIZE=2><SPAN LANG='es-MX'><B> 
                                    NOMBRE Y FIRMA DEL JEFE DIRECTO </B></SPAN></FONT></FONT></FONT></P>
                                    <P LANG='es-ES' CLASS='western' ALIGN=CENTER><FONT SIZE=3><FONT FACE='Arial, sans-serif'><FONT SIZE=2><SPAN LANG='es-MX'><B> 
                                        email
                                        </B></SPAN></FONT></FONT></FONT>
                                    </P>
                                </TD>
                            </TR>
                            <TR VALIGN=BOTTOM>
                                <TD COLSPAN=3 WIDTH=203 HEIGHT=9 STYLE='border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in' >
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>
                                    </P>
                                </TD>
                                <TD WIDTH=1 STYLE='border-top: none; border-bottom: none; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in' >
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>
                                    </P>
                                </TD>
                                <TD COLSPAN=3 WIDTH=225 STYLE='border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in' >
                                    <P LANG='es-MX' CLASS='western' ALIGN=CENTER>
                                    </P>
                                </TD>
                                <TD WIDTH=1 STYLE='border-top: none; border-bottom: none; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0in' >
                                    <P LANG='es-MX' CLASS='western' ALIGN=LEFT><BR>
                                    </P>
                                </TD>
                                <TD COLSPAN=3 WIDTH=207 STYLE='border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding: 0in 0.05in' >
                                    <P LANG='es-MX' CLASS='western' ALIGN=CENTER><BR>
                                    </ P >
                                </ TD >
                            </ TR >
                        </ TABLE >
                        <P LANG='es-ES' CLASS='western' ALIGN=LEFT><BR>
                        </P>
                        <P LANG='es-ES' CLASS='western' ALIGN=LEFT><BR>
                        </P>
                        <P LANG='es-ES' CLASS='western' ALIGN=CENTER><BR>
                        </P>
                        <P LANG='es-ES' CLASS='western' ALIGN=LEFT><FONT FACE='Arial, sans-serif'><FONT SIZE=3> </FONT></FONT></P>
                        <DIV TYPE=FOOTER>
                            <P LANG='es-MX' ALIGN=LEFT STYLE='margin-top: 0.16in'><SPAN ID='Frame1' DIR='LTR' STYLE='float: left; width: 2.74in; height: 0.74in; border: none; padding: 0in; background: #ffffff' >
                                <P LANG='es-ES' CLASS='western' ALIGN=LEFT><FONT SIZE=3><FONT COLOR='#0000ff'><U><A HREF='http://www.tasisoft.com/'>
                                    <FONT FACE='Arial, sans-serif'><FONT SIZE=1 STYLE='font-size: 8pt' > www.tasisoft.com </FONT></FONT></A></U></FONT><FONT FACE='Arial, sans-serif'><FONT SIZE=1 STYLE='font-size: 8pt' >
                                    </FONT></FONT></FONT>
                                </P>
                                </SPAN>
                                 
                            </P>
                        </DIV>";
            }
        }
    }
}