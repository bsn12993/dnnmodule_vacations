﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.VacationModule.Models
{
    public class MainModel
    {
        public string FileBase64 { get; set; }
        public AccessToken AccessToken { get; set; }

        private MainModel()
        {
            AccessToken = new AccessToken();
        }

        static MainModel()
        {
 
        }

        private static MainModel instance = new MainModel();
        public static MainModel GetInstance()
        {
            if (instance == null) return new MainModel();
            else return instance;
        }
    }
}