﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.VacationModule.BD
{
    public class ConnectionBD
    {
        /// <summary>
        /// Determina se la cadena es local
        /// </summary>
        public string LocalEnvionment = "Local";
        /// <summary>
        /// Determina si la cadena es del servidor
        /// </summary>
        public string ServerEnvionment = "Server";
        /// <summary>
        /// Determina si la cadena es del Test
        /// </summary>
        public string TestEnvionment = "Test";

        /// <summary>
        /// Metodo que retorna la cadena de conexión dependiendo del ambiente
        /// </summary>
        /// <param name="envionment"></param>
        /// <returns></returns>
        public string GetConnectionLocalString(string envionment)
        {
            if (envionment.Equals(this.LocalEnvionment))
                return "Data Source=(local);Initial Catalog=dnndev;User ID=sa;Password=123";
            else if (envionment.Equals(this.TestEnvionment))
                return "Data Source=198.12.158.210\\SQLWKG2008R2;Initial Catalog=Tasinet_Test;User ID=sa;Password=tasisoft01+";
            else
                return "Data Source=198.12.158.210\\SQLWKG2008R2;Initial Catalog=Tasinet2;User ID=sa;Password=tasisoft01+";
        }
    }
}